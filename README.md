# Сервис уведомлений

## Стек используемых технологий

- Python3.9
- Django, Django REST Framework
- PostgreSQL
- Celery
- Redis
- Docker, docker-compose

Зависимости проекта указаны в `api/requirements.txt`

## Запуск проекта

Сборка и запуск контейнеров Docker:

`docker-compose up --no-deps --build`

Применение миграций:

`docker-compose run api python api/manage.py migrate`

## Документация

Документация API доступна по адресу `/docs/`. Описание реализованных методов в формате OpenAPI находится в корне репозитория.

## Выполненные дополнительные задания

- подготовить docker-compose для запуска всех сервисов проекта одной командой (пункт 3)
- сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io (пункт 5)

