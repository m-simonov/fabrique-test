from django.urls import path
from . import views


urlpatterns = [
    path('clients/', views.ClientCreateView.as_view(), name='clients'),
    path('clients/<int:pk>/', views.ClientUpdateDeleteView.as_view(), name='client'),
    path('notifications/', views.NotificationCreateView.as_view(),
         name='notifications'),
    path('notifications/<int:pk>/',
         views.NotificationUpdateDeleteView.as_view(), name='notification'),
    path('notifications/statistic/',
         views.NotificationStatisticListView.as_view(), name='notifications_statistic'),
    path('notifications/statistic/<int:pk>/',
         views.NotificationStatisticDetailView.as_view(), name='notification_statistic'),
]
