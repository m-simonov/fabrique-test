from dateutil import parser

import pytz
from rest_framework import generics

from . import models, serializers, tasks


class ClientCreateView(generics.CreateAPIView):
    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all()


class ClientUpdateDeleteView(generics.UpdateAPIView, generics.DestroyAPIView):
    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all()


class NotificationCreateView(generics.CreateAPIView):
    serializer_class = serializers.NotificationSerializer
    queryset = models.Notification.objects.all()

    def post(self, request, *args, **kwargs):
        notification = super().post(request, *args, **kwargs)
        notification_id = notification.data.get('id')
        start = notification.data.get('start')
        end = notification.data.get('end')
        message_text = notification.data.get('message')
        filter = notification.data.get('filter')

        filtered_clients = models.Client.objects.filter(**filter)

        for client in filtered_clients:
            client_timezone = pytz.timezone(client.timezone)
            start_t = parser.parse(start).replace(tzinfo=None)
            end_t = parser.parse(end).replace(tzinfo=None)
            start_utc = client_timezone.localize(start_t).astimezone(pytz.utc)
            end_utc = client_timezone.localize(end_t).astimezone(pytz.utc)

            message = models.Message.objects.create(
                sending_status='CREATED',
                notification=models.Notification.objects.get(
                    id=notification_id),
                client=client
            )

            tasks.send_message.apply_async(
                (message_text, notification.data.get(
                    'id'), client.id, client.phone_number, message.id),
                eta=start_utc,
                expires=end_utc,
                retry=True,
            )
        return notification


class NotificationUpdateDeleteView(generics.UpdateAPIView, generics.DestroyAPIView):
    serializer_class = serializers.NotificationSerializer
    queryset = models.Notification.objects.all()


class NotificationStatisticListView(generics.ListAPIView):
    serializer_class = serializers.NotificationStatisticListSerialzier
    queryset = models.Notification.objects.all()


class NotificationStatisticDetailView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.NotificationStatisticListSerialzier
    queryset = models.Notification.objects.all()
