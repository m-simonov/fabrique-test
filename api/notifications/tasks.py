import json

import requests
from api.celery import app
from api.settings import JWT_TOKEN
from celery import Task

from . import models


class NotificationTask(Task):
    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        message = models.Message.objects.get(id=args[4])
        message.sending_status = status
        message.save()


@app.task(base=NotificationTask)
def send_message(message_text, notification_id, client_id, client_phone, message_id):
    url = f"https://probe.fbrq.cloud/v1/send/{message_id}"
    headers = {
        "Accept": "application/json",
        "Authorization": f"Bearer {JWT_TOKEN}",
        "Content-Type": "application/json"
    }
    data = {
        "id": int(message_id),
        "phone": int(client_phone),
        "text": str(message_text)
    }
    resp = requests.post(url=url, headers=headers, data=json.dumps(data))

    return True
