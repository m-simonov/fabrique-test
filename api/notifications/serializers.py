import re
import pytz
from collections import defaultdict
from itertools import groupby
from operator import itemgetter

from rest_framework import serializers

from . import models


class ClientSerializer(serializers.ModelSerializer):

    def validate(self, data):
        phone_number = data.get('phone_number')
        if bool(re.search("^7[0-9]{10}$", phone_number)) == False:
            raise serializers.ValidationError(
                "The phone number entered is not valid.")
        try:
            pytz.timezone(data.get('timezone'))
        except:
            raise serializers.ValidationError(
                "The timezone entered is not valid.")
        return super().validate(data)

    class Meta:
        model = models.Client
        fields = '__all__'


class FilterSerializer(serializers.Serializer):
    phone_code = serializers.CharField(max_length=3, required=False)
    tag = serializers.CharField(required=False)


class NotificationSerializer(serializers.ModelSerializer):
    filter = FilterSerializer()

    class Meta:
        model = models.Notification
        fields = '__all__'


class MessageListSerialzier(serializers.ModelSerializer):

    class Meta:
        model = models.Message
        fields = '__all__'


class NotificationStatisticListSerialzier(serializers.ModelSerializer):
    messages = serializers.SerializerMethodField()

    def get_messages(self, obj):
        messages = models.Message.objects.filter(
            notification_id=obj.id).order_by('sending_status')
        serializer = MessageListSerialzier(messages, many=True)
        serialized = serializer.data
        result = defaultdict(list)
        for key, value in groupby(serialized, key=itemgetter('sending_status')):
            for k in value:
                result[key].append(k)
        return result

    class Meta:
        model = models.Notification
        fields = '__all__'
