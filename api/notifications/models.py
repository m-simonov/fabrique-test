from django.db import models


class Notification(models.Model):
    start = models.DateTimeField()
    end = models.DateTimeField()
    message = models.TextField()
    filter = models.JSONField()

    def __str__(self):
        return f'{self.message[:10]}'
    
    class Meta:
        db_table = 'notification'


class Client(models.Model):
    phone_number = models.CharField(max_length=11, unique=True)
    phone_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=24)
    timezone = models.CharField(max_length=24)

    def __str__(self):
        return f'{self.phone_number}'

    class Meta:
        db_table = 'client'


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    sending_status = models.CharField(max_length=120)
    notification = models.ForeignKey(
        Notification,
        related_name='messages',
        on_delete=models.CASCADE,
    )
    client = models.ForeignKey(
        Client,
        related_name='clients',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.created_at}'
    
    class Meta:
        db_table = 'message'
