from django.contrib import admin

from .models import Notification, Client, Message


class NotificationAdmin(admin.ModelAdmin):
    pass


class ClientAdmin(admin.ModelAdmin):
    pass


class MessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(Notification, NotificationAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Message, MessageAdmin)
