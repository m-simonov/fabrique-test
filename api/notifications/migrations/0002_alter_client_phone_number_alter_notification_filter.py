# Generated by Django 4.1 on 2022-08-09 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.CharField(max_length=11, unique=True),
        ),
        migrations.AlterField(
            model_name='notification',
            name='filter',
            field=models.JSONField(),
        ),
    ]
